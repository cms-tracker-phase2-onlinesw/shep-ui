[![pipeline status](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/-/commits/master)
[![coverage report](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/badges/master/coverage.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/-/commits/master)

# shep-ui
A web-based UI for the the Shep API server

## Docker Container
### Supported Architectures
Currently the `shep-ui` image can only run on `x86_64` machines.
### Usage
Here are some example snippets to help you get started creating a container. This container should be used as part of the Shep stack, in conjuction with at least a `shep-api-server` and database. See [docs](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/deployment-and-documentation) for an example of the full stack.
#### docker
```shell
docker create \
    --name=shep-ui \
    --restart on-failure \
    -e ENVIRONMENT=development \
    -p 5000:5000 \
    gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/shep-ui
```
#### docker-compose
```yaml
---
version: "3"

services:
    shep-ui:
        image: gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/shep-ui:latest
        container_name: shep-ui
        restart: on-failure
        depends_on:
            - shep-api-server
        environment:
            ENVIRONMENT: development
        ports:
            - 5000:5000
        volumes:
            - staticfiles_volume:/var/staticfiles

volumes:
    staticfiles_volume:
```
### Parameters
| Parameter | Usage |
| ---       | ---   |
| -e ENVIRONMENT=development | Option for choosing the backend web server for the container. Options are `development` and `production` |
| -p 5000:5000 | Port for web interface. |
