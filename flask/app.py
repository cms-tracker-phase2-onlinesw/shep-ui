from flask import Flask, render_template, request, jsonify, session, Response, redirect
import requests
import json
from keycloak import Client
from keycloak.extensions.flask import AuthenticationMiddleware


app = Flask(__name__)
app.config['SECRET_KEY'] = 'EYxuFcNqGamVU78GgfupoO5N4z2xokA58XtL0ag'
# api.wsgi = AuthenticationMiddleware(
#     api.wsgi,
#     api.config,
#     api.session_interface,
#     callback_url="http://localhost:5000/login/callback",
#     redirect_uri="/",
#     logout_uri="/logout"
# )

API_URL = "http://shep-api:3000/api/v1"
DEBUG = True
keycloak_client = Client(
    callback_uri='http://localhost:5000/manager/login/callback',
)


def api_redirect(endpoint):
    if request.json:
        response = requests.request(
            request.method, "{}/{}".format(API_URL, endpoint),
            json=request.json, headers=request.headers)
        try:
            response_data = response.json()
        except json.decoder.JSONDecodeError:
            response_data = {}
    else:
        response = requests.request(request.method, "{}/{}".format(API_URL, endpoint), headers=request.headers, data=request.form.to_dict())
        try:
            response_data = response.json()
        except json.decoder.JSONDecodeError:
            response_data = {}
    return jsonify(response_data)


@app.route("/")
@app.route("/manager/")
def index():
    print(session)
    return render_template("index.html")


@app.route("/manager/board/<uuid>/", methods=['GET', 'POST'])
def boards(uuid):
    context = {"uuid": uuid}
    context['board'] = requests.get("{}/boards/{}".format(API_URL, uuid)).json()
    return render_template("board.html", context=context)


@app.route("/manager/application/<uuid>/", methods=['GET', 'POST'])
def applications(uuid):
    context = {"uuid": uuid}
    context['application'] = requests.get("{}/applications/{}".format(API_URL, uuid)).json()
    return render_template("application.html", context=context)


# the logging in happens at this stage
@app.route('/manager/login', methods=['GET'])
def login():
    """ Initiate authentication """
    # creating an authentication call
    # auth_url is the url containing the authentication endpoint with the configurations
    # state contains a random UUID
    auth_url, state = keycloak_client.login()
    # initializing the state session
    session['state'] = state
    # sending the request
    return redirect(auth_url)


# the redirect uri is handeled here
@app.route('/manager/login/callback', methods=['GET'])
def login_callback():
    """ Authentication callback handler """
    # get the state value from our request and validate it in current session
    state = request.args.get('state', 'unknown')
    _state = session.pop('state', None)
    if state != _state:
        return Response('Invalid state', status=403)

    # fetch code value from our request to auth server
    code = request.args.get('code')
    # make a callback call to get token for our code value
    # the reponse value is a JSON dict with different attributes such as
    # access_token, refresh_token, etc.
    response = keycloak_client.callback(code)

    # retrieve access token from our response
    access_token = response["access_token"]
    print(response)

    # using access token make request to auth server to fetch user parameters
    userinfo = keycloak_client.fetch_userinfo(access_token)

    # put user information dict returned by OAuth server into our session structure
    session["user"] = userinfo
    session["access_token"] = access_token
    session["refresh_token"] = response["refresh_token"]
    print(session)

    # return userinfo to upstream caller
    # return jsonify(userinfo)
    return redirect("/manager/")


@app.route("/manager/logout")
def logout():
    keycloak_client.logout(session["access_token"], session["refresh_token"])
    session.pop("access_token")
    session.pop("refresh_token")
    session.pop("user")
    print(session)
    return redirect("/manager/")


if __name__ == '__main__':
    if DEBUG:
        app.add_url_rule(
            "/api/v1/<path:endpoint>", view_func=api_redirect,
            methods=['POST', 'GET', 'DELETE', 'PATCH', 'PUT']
        )
    app.run("0.0.0.0", 5000, True)
