function generateDeviceInfoDiv() {
    var content_div = $("#content-device-info");
    $.get({
        url: `/api/v1/applications/${uuid}/devices`,
        success: function (response) {
            for (board of response) {
                content_div.append($("<h4>", {text: `${board.ip}`}));
                for (var device in board.data) {
                    $("<h5>", {text: device}).appendTo(content_div);
                    $("<div>", {
                        html: `<table class="table small device-info" id="${board.uuid}${device}"></table>`
                    }).appendTo(content_div);
                    generateDeviceInfoTable(board.uuid, device, $(`table.device-info#${board.uuid}${device}`));
                };
                content_div.append($("<div>", {class: "divider"}));
                $('table.device-info').each(function () {
                    $(this).trigger("refresh");
                });
            }

        }
    });
}

function parseFormOutput(response, output) {
    for (board of response) {
        output.append(board.ip);
        output.append(tabulateCommandOutput(board.data));
    }
}

function parseTransitionOutput(response, output) {
    for (board of response) {
        output.append(board.ip);
        var output_table = $("<table>", {class: "table"});
        for (key in board.data) {
            var row = $("<tr>")
            row.append($("<td>", {text: key}));
            if (key == "commands") {
                var cell = $("<td>");
                for (command of board.data[key]) {
                    cell.append(tabulateCommandOutput(command));
                }
                row.append(cell);
            } else {
                row.append($("<td>", {text: board.data[key]}));
            }
            output_table.append(row);
        }
        output.append(output_table);
    }
}

$(document).ready(function() {
    // --- Set up page --
    hideAll($('div.content.tab'));
    $("div#Overview").prop('hidden', false);
    generateDeviceList(`/api/v1/applications/${uuid}/`);
    generateDeviceInfoDiv();

    $.get({
        url: `/api/v1/applications/${uuid}/lease`,
        success: function (response) {
            console.log(response);
            if (!localStorage.getItem("SUPERVISOR_ID")) {
                localStorage.setItem("SUPERVISOR_ID", generateId(20));
            } else {
                claimed_all = true;
                for (board of response) {
                    if (localStorage.getItem("SUPERVISOR_ID") != board.data.supervisor) {
                        claimed_all = false;
                    }
                }
                if (claimed_all) {
                    var button = $("button#claim");
                    button.removeClass("btn-primary").addClass("btn-success").html(`<i class="fa-wrapper fa fa-lock pad-right"></i>claimed`);
                    button.attr("data-tooltip", "Release this resource");
                    $("button#renew").attr("hidden", false);
                }
            }
        }
    });

    // --- Listeners ---
    $('div#function-selector.tab-container li').click(function () {
        hideAll($('div.content.tab'));
        $("div#"+$(this).text()).prop('hidden', false);
        $('div#function-selector.tab-container li').removeClass('selected');
        $(this).addClass("selected");
        $('table.device-info').each(function () {
            $(this).trigger("refresh");
        });
        // setStatefulElements($("div#FSMs"));
    });

    $("body").on("click", "button#claim", function () {
        var button = $(this);
        if ($(this).hasClass("btn-primary")) {
            $.ajax({
                method: "PUT",
                url: `/api/v1/applications/${uuid}/lease/obtain`,
                contentType: "application/json",
                data: JSON.stringify({
                    "supervisor": localStorage.getItem("SUPERVISOR_ID"),
                    "duration": 300
                }),
                success: function (data, textStatus, jqXHR) {
                    button.removeClass("btn-primary").addClass("btn-success").html(`<i class="fa-wrapper fa fa-lock pad-right"></i>claimed`);
                    button.attr("data-tooltip", "Release this resource");
                    $("button#renew").attr("hidden", false);
                    console.log(data);
                    var lease = {}
                    for (board of data) {
                        lease[board.uuid] = board.data.lease;
                        localStorage.setItem(`lease_${board.uuid}`, board.data.lease);                        
                    }
                    localStorage.setItem(`lease_${uuid}`, JSON.stringify(lease));
                },
            });
        } else {
            $.ajax({
                method: "PUT",
                url: `/api/v1/applications/${uuid}/lease/retire`,
                contentType: "application/json",
                headers : {"lease": localStorage.getItem(`lease_${uuid}`)},
                data: JSON.stringify({}),
                success: function (data, textStatus, jqXHR) {
                    button.removeClass("btn-success").addClass("btn-primary").html(`<i class="fa-wrapper fa fa-unlock pad-right"></i>claim all`);
                    button.attr("data-tooltip", "Claim this resource");
                    $("button#renew").attr("hidden", true);
                },
            });
        }
    });

    $("body").on("click", "button#renew", function () {
        $.ajax({
            method: "PUT",
            url: `/api/v1/applications/${uuid}/lease/renew`,
            contentType: "application/json",
            headers : {"lease": localStorage.getItem(`lease_${uuid}`)},
            data: JSON.stringify({
                "lease": localStorage.getItem(`lease_${uuid}`),
                "duration": 300
            })
        });
    });

    deviceCheckboxClick(`/api/v1/applications/${uuid}/commands`);
    commandDropDownMenu(`/api/v1/applications/${uuid}/commands`);
    commandSendForm(parseFormOutput, `/api/v1/applications/${uuid}/`);

    // Build the FSMs section of the page
    buildFSMContentDiv(`/api/v1/applications/${uuid}/`);
    // --- Listeners ---
    addFSMListeners(`/api/v1/applications/${uuid}/`, parseTransitionOutput);
});
