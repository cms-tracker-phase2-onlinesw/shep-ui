function generateDeviceInfoDiv() {
    var content_div = $("#content-device-info");
    $.get({
        url: `/api/v1/boards/${uuid}/devices`,
        success: function (response) {
            for (var device in response) {
                $("<h5>", {
                    text: device
                }).appendTo(content_div);
                $("<div>", {
                    html: `<table class="table small device-info" id="${device}"></table>`
                }).appendTo(content_div);
                generateDeviceInfoTable(uuid, device, $(`table.device-info#${device}`));
            };
            $('table.device-info').each(function () {
                $(this).trigger("refresh");
            });
        }
    });
}

function parseFormOutput(response, output) {
    output.append(tabulateCommandOutput(response));
}

function parseTransitionOutput(response, output) {

    var startTime = new Date(response.startTime * 1000);
    var durationSuffix = "Took " + response.duration.toFixed(1) + " seconds";
    durationSuffix +=  " (started at " + startTime.getHours() + ":" + startTime.getMinutes() + ":" + startTime.getSeconds() + ").";

    var summaryNode = $("<div>", {style: "flex-basis: 100%"});
    if (response.state == "Done") {
        summaryNode.append($("<p>", {style: "flex-basis: 100%"}).html("<b>Transition completed successfully!</b> " + durationSuffix));
    } else if (response.state == "Warning") {
        summaryNode.append($("<p>", {style: "flex-basis: 100%"}).html("<b>Transition completed, but warning issued!</b> " + durationSuffix));
    } else if (response.state == "Error") {
        summaryNode.append($("<p>", {style: "flex-basis: 100%"}).html("<b>ERROR occurred in '" + response.commands.slice(-1)[0].path.split(".").slice(-1)[0] + "'!</b> " + durationSuffix));
    } else {
        summaryNode.append($("<p>", {style: "flex-basis: 100%"}).html("Transition in progress (" + response.duration.toFixed(1) + " seconds so far)"));
    }
    output.append(summaryNode)

    output.append($("<div>", {style: "flex-basis: 100%"}).append($("<p>", {style: "margin-bottom: 0"}).text("Details of individual commands:")));
    var commandStatusList = $("<ol>", {style: "flex-basis: 100%; margin-top: 0"});
    for (commandStatus of response.commands) {
        var listItem = $("<li>");
        listItem.append($("<b>").text(commandStatus.path.split(".").slice(-1)[0]));
        if (commandStatus.state == "Warning") {
            listItem.append($("<br>"));
            listItem.append($("<i>").text("Note: ").append($("<b>").text("WARNING issued!")));
        } else if (commandStatus.state == "Error") {
            listItem.append($("<br>"));
            listItem.append($("<i>").text("Note: ").append($("<b>").text("Returned ERROR!")));
        }

        var listItemDetails = $("<table>", {class: "table"});

        for (const [index, x] of commandStatus.messages.entries()) {
            var row = $("<tr>");
            if (index == 0) {
                row.append($("<td>", {rowspan: commandStatus.messages.length, style: "text-align:left"}).text("Messages:"));
            }
            row.append($("<td>", {style: "text-align:left"}).text(x[1]))
            listItemDetails.append(row);
        }

        if (commandStatus.state == "Done" || commandStatus.state == "Warning") {
            var resultRow = $("<tr>");
            resultRow.append($("<td>", {style: "text-align:left"}).text("Result:"))
            if (isObject(commandStatus.result)) {
                resultRow.append($("<td>", {style: "text-align:left"}).append(
                    $("<a>", {
                        text: commandStatus.result.name,
                        download: commandStatus.result.name,
                        href: 'data:application/octet-stream;base64,' + commandStatus.result.contents
                    })
                ));
            } else {
                resultRow.append($("<td style='text-align:left'>").text(commandStatus.result));
            }
            if (commandStatus.result != "") {
                listItemDetails.append(resultRow);
            }
        }
        listItem.append(listItemDetails);

        commandStatusList.append(listItem);
    }
    output.append(commandStatusList);
}

$(document).ready(function() {
    // --- Set up page ---
    hideAll($('div.content.tab'));

    generateDeviceInfoDiv();
    $("div#Overview").prop('hidden', false);
    generateDeviceList(`/api/v1/boards/${uuid}/`);

    $.get({
        url: `/api/v1/boards/${uuid}/lease`,
        success: function (response) {
            console.log(response);
            if (!localStorage.getItem("SUPERVISOR_ID")) {
                localStorage.setItem("SUPERVISOR_ID", generateId(20));
            } else {
                if (localStorage.getItem("SUPERVISOR_ID") == response.supervisor) {
                    var button = $("button#claim");
                    button.removeClass("btn-primary").addClass("btn-success").html(`<i class="fa-wrapper fa fa-lock pad-right"></i>claimed`);
                    button.attr("data-tooltip", "Release this resource");
                    $("button#renew").attr("hidden", false);
                }
            }
        }
    });


    // --- Listeners ---
    $("body").on("click", "div#function-selector.tab-container li", function () {
        hideAll($('div.content.tab'));
        $("div#"+$(this).text()).prop('hidden', false);
        $('div#function-selector.tab-container li').removeClass('selected');
        $(this).addClass("selected");
        $('table.device-info').each(function () {
            $(this).trigger("refresh");
        });
        setStatefulElements($("div#FSMs"), `/api/v1/boards/${uuid}/`);
    });
    $("body").on("refresh", "table.device-info", function () {
        var table = $(this);
        var device = $(this).attr("id");
        generateDeviceInfoTable(uuid, device, table);
    });
    // Handler for claim button
    $("body").on("click", "button#claim", function () {
        var button = $(this);
        if ($(this).hasClass("btn-primary")) {
            $.ajax({
                method: "PUT",
                url: `/api/v1/boards/${uuid}/lease/obtain`,
                contentType: "application/json",
                data: JSON.stringify({
                    "supervisor": localStorage.getItem("SUPERVISOR_ID"),
                    "duration": 600
                }),
                success: function (data, textStatus, jqXHR) {
                    button.removeClass("btn-primary").addClass("btn-success").html(`<i class="fa-wrapper fa fa-lock pad-right"></i>claimed`);
                    button.attr("data-tooltip", "Release this resource");
                    $("button#renew").attr("hidden", false);
                    localStorage.setItem(`lease_${uuid}`, data.lease);
                },
            });
        } else {
            $.ajax({
                method: "PUT",
                url: `/api/v1/boards/${uuid}/lease/retire`,
                contentType: "application/json",
                headers: {lease: localStorage.getItem(`lease_${uuid}`)},
                data: JSON.stringify({}),
                success: function (data, textStatus, jqXHR) {
                    button.removeClass("btn-success").addClass("btn-primary").html(`<i class="fa-wrapper fa fa-unlock pad-right"></i>claim`);
                    button.attr("data-tooltip", "Claim this resource");
                    $("button#renew").attr("hidden", true);
                },
            });
        }
    });
    // Handler for renew button
    $("body").on("click", "button#renew", function () {
        $.ajax({
            method: "PUT",
            url: `/api/v1/boards/${uuid}/lease/renew`,
            contentType: "application/json",
            headers: {lease: localStorage.getItem(`lease_${uuid}`)},
            data: JSON.stringify({
                "duration": 600
            })
        });
    });

    // --- Listeners ---
    deviceCheckboxClick(`/api/v1/boards/${uuid}/commands`);

    commandDropDownMenu(`/api/v1/boards/${uuid}/commands`);

    commandSendForm(parseFormOutput, `/api/v1/boards/${uuid}/`);

    // Build the FSMs section of the page
    buildFSMContentDiv(`/api/v1/boards/${uuid}/`);
    // --- Listeners ---
    addFSMListeners(`/api/v1/boards/${uuid}/`, parseTransitionOutput);
});
