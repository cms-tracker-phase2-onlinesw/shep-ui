function buildBoardsTable(table) {
    $.ajax({
        type: 'GET',
        url: "/api/v1/boards",
        success: function(response) {
            table.empty();
            for (board of response.results) {
                var row = $("<tr>");
                var uuid_short = board.uuid.substring(0, 8);
                row.append($("<td>").html(`<a href="/manager/board/${board.uuid}">${uuid_short}</a>`));
                row.append($("<td>").text(board.ip));
                row.append($("<td>").text(board.type));
                var timestamp = new Date(board.date_registered);
                row.append($("<td>").text(timestamp.toGMTString()));
                if (board.online) {
                    row.append($("<td>", {
                        class: "text-success",
                        text: "Online"
                    }));
                } else {
                    row.append($("<td>", {
                        class: "text-danger",
                        text: "Offline"
                    }));
                }
                row.append($("<td>").append(
                    $("<div>", {class: "btn-group"}).append($("<a>", {href: "#update-board-modal"}).append(
                        $("<button>", {
                            class: "btn-small p-0 edit-board",
                            html: `<i class="fa-wrapper fa fa-edit"></i>`
                        }))
                    ).append(
                        $("<button>", {class: "btn-danger btn-small p-0 delete-board", html: `<i class="fa-wrapper fa fa-trash"></i>`})
                    )
                ));
                row.append($("<td hidden>").text(board.uuid));
                table.append(row);
            }
        }
    });
}

function buildApplicationsTable(table) {
    $.ajax({
        type: 'GET',
        url: "/api/v1/applications",
        success: function(response) {
            table.empty();
            for (application of response.results) {
                var row = $("<tr>");
                row.append($("<td>").html(`<a href="/manager/application/${application.uuid}">${application.name}</a>`));
                var timestamp = new Date(application.date_registered);
                row.append($("<td>").text(timestamp.toGMTString()));
                var tag_div = $("<div/>", {
                    class: "tag-container"
                });
                for (board of application.boards) {
                    tag_div.append($("<div/>", {
                        class: "tag",
                        text: board
                    }));
                }
                row.append($("<td>").append(tag_div));
                row.append($("<td>").append(
                    $("<div>", {class: "btn-group"}).append(
                        $("<a>", {href: "#update-application-modal"}).append(
                            $("<button>", {
                                class: "btn-small p-0 edit-application",
                                html: `<i class="fa-wrapper fa fa-edit"></i>`
                            })
                        )
                    ).append(
                        $("<button>", {class: "btn-danger btn-small p-0 delete-application", html: `<i class="fa-wrapper fa fa-trash"></i>`})
                    )
                ));
                row.append($("<td hidden>").text(application.uuid));
                table.append(row);
            }
        }
    });
}


$(document).ready(function() {
    buildBoardsTable($('table#boards-table tbody'));
    buildApplicationsTable($('table#applications-table tbody'));

    $("body").on("click", "#new-board-modal button#confirm-board", function() {
        var form = $(this).closest("div.modal-content").find("form")[0];
        var formData = new FormData(form);
        var formObject = {};
        formData.forEach((value, key) => formObject[key] = value);
        $.post({
            url: "/api/v1/boards",
            data: $(form).serialize(),
            success: function(response) {
                buildBoardsTable($('table#boards-table tbody'));
            }
        });
    });

    $("body").on("click", "button.delete-board", function() {
        const uuid = $(this).closest("td").next().text()
        $.ajax({
            type: 'DELETE',
            url: `/api/v1/boards/${uuid}`,
            success: function(response) {
                buildBoardsTable($('table#boards-table tbody'));
            }
        });
    });

    $("body").on("click", "button.edit-board", function() {
        const uuid = $(this).closest("td").next().text();
        $.ajax({
            type: 'GET',
            url: `/api/v1/boards/${uuid}`,
            success: function(response) {
                $("#update-board-modal form input.ip-input").val(response.ip);
                $("#update-board-modal form input.type-input").val(response.type);
                $("#update-board-modal div.uuid-row").text(uuid);
            }
        });
    });

    $("body").on("click", "#update-board-modal button#confirm-board", function() {
        const uuid = $(this).closest("div.modal-content").find("div.uuid-row").text();
        var form = $(this).closest("div.modal-content").find("form")[0];
        var formData = new FormData(form);
        var formObject = {};
        formData.forEach((value, key) => formObject[key] = value);
        $.ajax({
            type: 'PATCH',
            url: `/api/v1/boards/${uuid}/`,
            data: $(form).serialize(),
            success: function(response) {
                buildBoardsTable($('table#boards-table tbody'));
            }
        });
    });

    $("body").on("click", "button.delete-application", function() {
        const uuid = $(this).closest("td").next().text()
        $.ajax({
            type: 'DELETE',
            url: `/api/v1/applications/${uuid}`,
            success: function(response) {
                buildApplicationsTable($('table#applications-table tbody'));
            }
        });
    });

    $("body").on("click", "button.edit-application", function() {
        const uuid = $(this).closest("td").next().text();
        selector = $("div#update-application-modal").find("select");
        selector.empty()
        $.get({
            url: "/api/v1/boards",
            success: function(response) {
                for (board of response.results) {
                    var uuid_short = board.uuid.substring(0, 7);
                    selector.append($("<option>", {text: `${board.uuid}`}));
                }
            }
        });
        $.ajax({
            type: 'GET',
            url: `/api/v1/applications/${uuid}`,
            success: function(response) {
                $("#update-application-modal form input.name-input").val(response.name);
                $("#update-application-modal form input.type-input").val(response.type);
                $("#update-application-modal div.uuid-row").text(uuid);
            }
        });
    });

    $("body").on("click", "a#new-application", function() {
        selector = $("div#new-application-modal").find("select");
        selector.empty()
        $.get({
            url: "/api/v1/boards",
            success: function(response) {
                for (board of response.results) {
                    var uuid_short = board.uuid.substring(0, 8);
                    selector.append($("<option>", {text: `${board.uuid}`}));
                }
            }
        });
    });

    $("body").on("click", "#new-application-modal button#confirm-application", function() {
        var form = $(this).closest("div.modal-content").find("form");
        var boards = form.find("select").val();
        var name = form.find("input").val();
        $.post({
            url: "/api/v1/applications",
            data: {
                name: form.find("input").val(),
                boards: `{${boards.join(",")}}`
            },
            success: function(response) {
                buildApplicationsTable($('table#applications-table tbody'));
            }
        });
    });

    $("body").on("click", "#update-application-modal button#confirm-application", function() {
        const uuid = $(this).closest("div.modal-content").find("div.uuid-row").text();
        var form = $(this).closest("div.modal-content").find("form");
        var boards = form.find("select").val();
        data = {name: form.find("input").val()};
        if (boards.length > 0) {
            data['boards'] = `{${boards.join(",")}}`;
        }
        console.log(data);
        $.ajax({
            type: "PATCH",
            url: `/api/v1/applications/${uuid}`,
            data: data,
            success: function(response) {
                console.log(response);
                buildApplicationsTable($('table#applications-table tbody'));
            }
        });
    });
});

document.addEventListener('keyup', function(e) {
    if(e.key === "Escape") {
        const modals = document.querySelectorAll('.modal-overlay');
        for (const modal of modals) {
            modal.click();
        }
    }
});
