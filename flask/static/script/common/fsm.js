// Function to build the FSM details table
function buildFSMTable(device, fsm, response, table) {
    // Get only the specific fsm of the given device
    var fsm_response = response[device][fsm];

    table.append($("<thead>").html("<th>State</th><th>Transition</th><th>End State</th><th>Commands</th>"));
    // Iterate over the FSM states
    for (state in fsm_response[3]) {
        if (jQuery.isEmptyObject(fsm_response[3][state])) {
            var row = $("<tr>");
            row.append($("<td>", {text: state}))
            row.append($("<td>", {html: "<i>No transitions</i>", colspan: 3}));
            table.append(row)
        } else {
            // Iterate over the available transitions for a given state
            let index = 0;
            for (transition in fsm_response[3][state]) {
                var row = $("<tr>");
                if (index == 0) {
                    row.append($("<td>", {rowspan: Object.keys(fsm_response[3][state]).length}).text(state));
                }
                row.append($("<td>", {text: transition}));
                row.append($("<td>", {text: fsm_response[3][state][transition][1]}));
                var tag_div = $("<div/>", {class: "tag-container"});
                for (command of fsm_response[3][state][transition][2]) {
                    tag_div.append($("<div/>", {class: "tag"}).text(command));
                }
                row.append($("<td>").append(tag_div));
                table.append(row);
                index++;
            }
        }
    }
}


function setStatefulElements(root_div, base_url) {
    $.get({
        url: base_url + "devices",
        success: function(response) {
            response = getApplicationResponse(response, deep=true);
            $.get({
                url: base_url + "fsms",
                success: function(fsms_response) {
                    fsms_response = getApplicationResponse(fsms_response, deep=true);
                    $.each(root_div.find("select"), (index, selector) => {
                        $(selector).html("<option hidden disabled selected value> -- select an option -- </option>");
                        $(selector).closest('div.row').next().find('div.col-9').empty();
                    });
                    for (device in response) {
                        for (fsm in fsms_response[device]) {
                            root_div.find(`div.${device}`).find(`div#${fsm}`).next().next().find("div.col-3.state").text(`${response[device].state}`); // Show current state in run div
                        }
                        var fsm = response[device].fsm;
                        if (fsm != null) {
                            var row_div = root_div.find(`div.fsm-device.${device} div.row#${fsm}`);
                            row_div.find(`div.col-1`).find("i").prop("hidden", false); // Tick next to FSM name
                            row_div.find(`div.col-6`).find("button#engage").removeClass("btn-success").addClass("btn-warning").find("span").text("Disengage"); // Set engage button to disengage instead
                            row_div.find(`div.col-6`).find("button#reset").prop("disabled", false); // Enable the reset button
                            var selector = row_div.next().next().find("select"); // Populate the transition selector
                            selector.closest("form").find('input.btn-primary').prop("disabled", true);
                            for (var transition in fsms_response[device][fsm][3][response[device].state]) {
                                selector.append(new Option(transition, transition));
                            }
                        }
                    }
                }
            });

        }
    });
}


function buildFSMContentDiv(base_url) {
    $.get({
        url: base_url + "fsms",
        success: function(response) {
            response = getApplicationResponse(response, deep=true);
            // Save keys for default selections
            const keys = Object.keys(response);
            root_div = $("div#FSMs");
            // Build tabs for each device on the board
            var device_list = $("<ul>");
            for (device in response) {
                device_list.append($("<li>", {id: device, html: `<div class="tab-item-content">${device}</div>`}))
            }
            root_div.append($("<div>", {class: "tab-container"}).append(device_list));
            // Iterate over the devices on the board and construc the tab content
            for (device in response) {
                device_div = $("<div>", {class: `content fsm-device ${device}`, id: device, hidden: true});
                // Build a config row for each FSM in the device
                for (fsm in response[device]) {
                    var fsm_div = $("<div>", {class: "row", id: fsm});
                    fsm_div.append($("<div>", {class: "col-1", html: `<h6><i class="fa fa-wrapper fa-check" hidden="true"></i></h6>`})); // Tick shown if this FSM is engaged
                    fsm_div.append($("<div>", {class: "col-2", html: `<h6>${fsm}</h6>`}));
                    // Control buttons
                    fsm_div.append(
                        $("<div>", {class: "col-6"}).append(
                            $("<div>", {class: "btn-group fsm-controls"}).append(
                                $("<button>", {
                                    class: `btn-success tooltip`,
                                    id: "engage",
                                    html: `<i class="fa fa-wrapper fa-power-off pad-right"></i><span>Engage</span>`,
                                    "data-tooltip": "Engage/disengage the state machine."
                                })
                            ).append(
                                $("<button>", {
                                    class: "btn-info tooltip",
                                    id: "reset",
                                    disabled: true,
                                    html: `<i class="fa fa-wrapper fa-undo pad-right"></i>Reset`,
                                    "data-tooltip": "Reset the state machine."
                                })
                            ).append(
                                $("<button>", {
                                    class: "btn-plain show-details tooltip",
                                    html: `<i class="fa fa-wrapper fa-chevron-down pad-right"></i>Details`,
                                    "data-tooltip": "Show the state machine details."
                                })
                            ).append(
                                $("<button>", {
                                    class: "btn-plain run-transition tooltip",
                                    html: `<i class="fa fa-wrapper fa-chevron-down pad-right"></i>Run`,
                                    "data-tooltip": "Configure and run a transition."
                                })
                            )
                        )
                    );
                    device_div.append(fsm_div);
                    // Build the FSM details table
                    device_div.append(
                        $("<div>", {class: "row", hidden: true}).append(
                            $("<table>", {class: "table", id: "fsm-details"})
                        )
                    );
                    buildFSMTable(device, fsm, response, device_div.find("table#fsm-details"));
                    // Build the content div that contains the run transition form
                    run_div = $("<div>", {class: "content runfsm", hidden: true});
                    run_div.append($("<div>", {class: "row"}).append(
                        $("<div>", {class: "col-3", text: "Current state:"})
                    ).append(
                        $("<div>", {class: "col-3 state", text: "None"})
                    ));
                    var run_form = $("<form class='transition_form'>", {});
                    // Add select element to chose transition
                    var selector = $("<select>", {class: `select transition-selector`, name: "transition"});
                    selector.html("<option hidden disabled selected value> -- select an option -- </option>");
                    run_form.append($("<div>", {class: "row level"}).append(
                        $("<div>", {class: "col-3", text: "Available transitions:"})
                    ).append(
                        $("<div>", {class: "col-9"}).append(selector)
                    ));
                    // Add placeholder div which will be populated with the config options
                    run_form.append($("<div>", {class: "row level"}).append(
                        $("<div>", {class: "col-3", text: "Parameters:"})
                    ).append(
                        $("<div>", {class: "col-9"})
                    ));
                    run_form.append($("<div>", {class: "row level"}).append(
                        $("<div>", {class: "col-3", html: `<input type="submit" class="btn-primary" value="Run transition" disabled/>`})
                    ));
                    run_div.append(run_form);
                    run_div.append($("<div>", {class: "divider"}));
                    // Add output div
                    run_div.append($("<div>", {class: "row level"}).append(
                        $("<div>", {class: "col-3 level-item", html: "<h6>Status</h6>"})
                    ).append(
                        $("<div>", {class: "col-9 level-item", html: `<progress class="transition-progress" value="0" max="1"></progress>`})
                    ));
                    run_div.append($("<div>", {class: "row level col-6 output", id: "command_output"}));
                    device_div.append(run_div);
                    device_div.append($("<div>", {class: "divider"}));

                }
                root_div.append(device_div);
            }
            root_div.find(`div.fsm-device.${keys[0]}`).prop("hidden", false);
            root_div.find(`div.tab-container li#${keys[0]}`).addClass("selected");
            // Get state information from board
            setStatefulElements(root_div, base_url);
        }
    });
}

function transitionError(error, device) {
    var transition_toast = $($("<div>", {class: "toast toast--warning toast--translucent animated fadeIn"}).append(
        $("<button>", {class: "btn-close"})
    ).append(
        $("<h6>", {text: `Error ${error.status}`})
    ).append(
        $("<p>", {text: error.responseText})
    ));
    transition_toast.insertAfter(`div.${device} form`);
    setTimeout(function() {
        transition_toast.remove();
    }, 10000);
}

function addFSMListeners(base_url, parsing_func) {
    // Device tab selector
    $("body").on("click", 'div#FSMs div.tab-container li', function () {
        hideAll($('div.fsm-device'));
        $("div.fsm-device."+$(this).text()).prop('hidden', false);
        $('div#FSMs div.tab-container li').removeClass('selected');
        $(this).addClass("selected")
    });
    // Show/hide the FSM details table
    $("body").on("click", ".show-details", function () {
        if ($(this).hasClass("selected")) {
            var table = $(this).closest("div.row").next();
            table.prop("hidden", true);
            $(this).removeClass("selected");
            $(this).find("i").removeClass("fa-chevron-up");
            $(this).find("i").addClass("fa-chevron-down");
        } else {
            var table = $(this).closest("div.row").next();
            table.prop("hidden", false);
            $(this).addClass("selected");
            $(this).find("i").removeClass("fa-chevron-down");
            $(this).find("i").addClass("fa-chevron-up");
        }
    });
    // Show/hide the run transition form
    $("body").on("click", "button.run-transition", function () {
        if ($(this).hasClass("selected")) {
            var form = $(this).closest("div.row").next().next();
            form.prop("hidden", true);
            $(this).removeClass("selected");
            $(this).find("i").removeClass("fa-chevron-up");
            $(this).find("i").addClass("fa-chevron-down");
        } else {
            var form = $(this).closest("div.row").next().next();
            form.prop("hidden", false);
            $(this).addClass("selected");
            $(this).find("i").removeClass("fa-chevron-down");
            $(this).find("i").addClass("fa-chevron-up");
        }
    });
    // Engage/disengage FSM
    $("body").on("click", "div.fsm-controls button#engage", function () {
        const device = $(this).closest("div.content").attr('id');
        const fsm = $(this).closest("div.row").attr('id');
        var button = this;
        if ($(this).find("span").text() == "Engage") {
            $.ajax({
                url: base_url + `fsms/${device}/${fsm}/engage`,
                method: 'PUT',
                contentType: "application/json",
                headers: {lease: localStorage.getItem(`lease_${uuid}`)},
                data: JSON.stringify({"parameters": false}),
                success: function(response) {
                    $(button).removeClass("btn-success").addClass("btn-warning").find("span").text("Disengage");
                    $(button).closest("div.row").find(`div.col-1`).find("i").prop("hidden", false);
                    $(button).next().prop("disabled", false);
                    setStatefulElements($("div#FSMs"), base_url);
                }
            });
        } else if ($(this).find("span").text() == "Disengage") {
            $.ajax({
                url: base_url + `fsms/${device}/${fsm}/disengage`,
                method: 'PUT',
                contentType: "application/json",
                headers: {lease: localStorage.getItem(`lease_${uuid}`)},
                data: JSON.stringify({"parameters": false}),
                success: function(response) {
                    $(button).removeClass("btn-warning").addClass("btn-success").find("span").text("Engage");
                    $(button).closest("div.row").find(`div.col-1`).find("i").prop("hidden", true);
                    $(button).next().prop("disabled", true);
                    setStatefulElements($("div#FSMs"), base_url);
                }
            });
        }
    });
    // Reset FSM
    $("body").on("click", "div.fsm-controls button#reset", function () {
        const device = $(this).closest("div.content").attr('id');
        const fsm = $(this).closest("div.row").attr('id');
        var button = this;
        $.ajax({
            url: base_url + `fsms/${device}/${fsm}/reset`,
            method: 'PUT',
            contentType: "application/json",
            headers: {lease: localStorage.getItem(`lease_${uuid}`)},
            data: JSON.stringify({"parameters": false}),
            success: function(response) {
                $(button).find("i").removeClass("fa-undo").addClass("fa-check");
                $(button).prop("disabled", true);
                setTimeout(function () {
                    $(button).find("i").removeClass("fa-check");
                    $(button).find("i").addClass("fa-undo");
                    $(button).prop("disabled", false);
                }, 2000);
                setStatefulElements($("div#FSMs"), base_url);
            }
        });
    });
    // Show config tables upons selecting a transition
    $("body").on("change", ".transition-selector", function() {
        const device = $(this).closest("div.content").parent().attr('id');
        const fsm = $(this).closest("div.content").prev().prev().attr('id');
        const state = $(this).closest("div.content").find("div.state").text();
        var selector = $(this);
        selector.closest("form").find('input.btn-primary').prop("disabled", false);
        $.ajax({
            type: 'GET',
            url: base_url + "fsms",
            success: function (response) {
                var config_row = selector.closest('div.row').next()
                config_row.find('div.col-9').empty();
                response = getApplicationResponse(response, deep=true);
                response[device][fsm][3][state][selector[0].value][2].forEach(command => {
                    var col = $("<div>", {class: "col", html: `<b>${command[0]}<b>`});
                    var config_table = $("<table>");
                    getConfigTable(device, command[0], config_table, base_url + "commands");
                    col.append(config_table);
                    config_row.find('div.col-9').append(col);
                })
            }
        });
    });
    // Submit form to run transition
    $("body").on("submit", ".transition_form", function(e) {
        e.preventDefault();
        const form = $(this);
        const device = $(this).closest("div.content").parent().attr('id');
        const fsm = $(this).closest("div.content").prev().prev().attr('id');
        const state = $(this).closest("div.content").find("div.state").text();
        var button = $(this).find('input.btn-primary');
        var output = form.closest('div').find('div.output');
        output.empty();
        button.prop("disabled", true);
        var formData = new FormData(this);
        var formObject = {};
        formData.forEach((value, key) => formObject[key] = value);
        const transition = formObject.transition;
        delete formObject.transition;
        $.get({
            url: base_url + "fsms",
            success: function (response) {
                var fsms_response = getApplicationResponse(response, deep=true);
                var requestObject = {"parameters": []};
                $.get({
                    url: base_url + "commands",
                    success: async function (response) {
                        response = getApplicationResponse(response, deep=true);
                        for (command of fsms_response[device][fsm][3][state][transition][2]) {
                            var command_options = {};
                            for (option in response[device][command[0]][2]) {
                                var form_value = formObject[`${command[0]}${option}`];
                                switch (response[device][command[0]][2][option][0]) {
                                    case "bool":
                                        command_options[option] = form_value == "true";
                                        break;
                                    case "uint":
                                        command_options[option] = parseInt(form_value);
                                        break;
                                    case "file":
                                        file_option = option;
                                        if (form_value.size == 0) {
                                            command_options[file_option] = {path: ""};
                                        } else {
                                            command_options[file_option] = {name: form_value.name};
                                            command_options[file_option]['contents'] = await getBase64(form_value);
                                        }
                                        break;
                                    default:
                                        command_options[option] = form_value;
                                        break;
                                }
                            }
                            requestObject["parameters"].push(command_options);
                        }
                        $.ajax({
                            url: base_url + `fsms/${device}/${fsm}/${state}/${transition}`,
                            method: 'PUT',
                            contentType: "application/json",
                            headers: {lease: localStorage.getItem(`lease_${uuid}`)},
                            data: JSON.stringify(requestObject),
                            success: function (response) {
                                var progress = 0;
                                var running = true;
                                function poll(){
                                    if (progress < 1 && running) {
                                        $.get({
                                            url:base_url + `fsms/${device}/${fsm}/${state}/${transition}`,
                                            success: function(response) {
                                                progress = getProgressFromResponse(response);
                                                form.closest("div.content").find("progress").val(progress);
                                                running = getRunningFromResponse(response);
                                                if (running == false) {
                                                    parsing_func(response, output);
                                                    setStatefulElements($("div#FSMs"), base_url);
                                                    button.prop("disabled", false);
                                                }
                                            },
                                            dataType: "json",
                                            complete: setTimeout(poll, 1000),
                                            timeout: 2000,
                                            error: function(error, status, httpError) {
                                                running = false;
                                                transitionError(error, device);
                                                button.prop("disabled", false);
                                            }
                                        });
                                    }
                                }
                                poll();
                            },
                            error: function (error, status, httpError) {
                                transitionError(error, device);
                                button.prop("disabled", false);
                            }
                        });
                    },
                    error: function (error, status, httpError) {
                        transitionError(error, device);
                        button.prop("disabled", false);
                    }
                });
            }
        });
    });
}
