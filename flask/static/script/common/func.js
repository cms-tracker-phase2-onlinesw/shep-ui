function hideAll(selector) {
    selector.prop('hidden', true);
}

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
      if ((encoded.length % 4) > 0) {
        encoded += '='.repeat(4 - (encoded.length % 4));
      }
      resolve(encoded);
    };
    reader.onerror = error => reject(error);
  });
}

function getConfigTable(device, command, html_table, url) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function(response) {
            response = getApplicationResponse(response, deep=true);
            response = response[device][command];
            options = response[2];
            $(html_table).append(
                $("<tr>").append(
                    $("<td>", {
                        colspan: "2",
                        style: `text-align:left;`
                    }).append($("<i>").text(response[1]))
                )
            );
            // console.log(response);
            for (option in options) {
                // console.log(option);
                var row = $("<tr>");
                row.append($("<td>", {text: option, class: "tooltip tooltip--left", "data-tooltip":options[option][0]}));
                // row.append($("<td>").html(options[option][0]));
                switch (options[option][0]) {
                    case "bool":
                        row.append($("<td>", {
                            html: `
                                <div class="form-ext-control">
                                    <label class="form-ext-toggle__label">
                                        <div class="form-ext-toggle">
                                            <input type='hidden' value='false' name='${command}${option}'>
                                            <input name='${command}${option}' type="checkbox" class="form-ext-input" value="true"/>
                                            <div class="form-ext-toggle__toggler"><i></i></div>
                                        </div>
                                    </label>
                                </div>
                                `,
                            class: "tooltip tooltip--right",
                            "data-tooltip":options[option][1]
                        }));
                        break;
                    case "uint":
                        row.append($("<td>" , {
                            html: `<input type="number" name='${command}${option}' value="1">`,
                            colspan: "2",
                            class: "tooltip tooltip--right",
                            "data-tooltip":options[option][1]
                        }));
                        break;
                    case "file":
                        row.append($("<td>", {
                            html: `<input type='file' name='${command}${option}'>`,
                            colspan: "2",
                            class: "tooltip tooltip--right",
                            "data-tooltip":options[option][1]
                        }));
                        break;
                    default:
                    case "string":
                        row.append($("<td>", {
                            html: `<input type='text' name='${command}${option}'>`,
                            colspan: "2",
                            class: "tooltip tooltip--right",
                            "data-tooltip":options[option][1]
                        }));
                        break;
                }
                $(html_table).append(row);
            }
        }
    });
}

function tabulateCommandOutput(output) {
    var table = $("<table>");
    table.addClass("table").addClass("striped");
    console.log(output);

    var startTime = new Date(output.startTime * 1000);
    var durationSuffix = "Took " + output.duration.toFixed(1) + " seconds";
    durationSuffix +=  " (started at " + startTime.getHours() + ":" + startTime.getMinutes() + ":" + startTime.getSeconds() + ").";
    var headerCell = $("<tr>");
    if (output.state == "Done") {
        headerCell.append($("<th colspan='2'>").text("Command completed successfully! " + durationSuffix));
    } else if (output.state == "Warning") {
        headerCell.append($("<th colspan='2'>").text("Command completed, but warning issued! " + durationSuffix));
    } else if (output.state == "Error") {
        headerCell.append($("<th colspan='2'>").text("ERROR occurred in command! " + durationSuffix));
    } else {
        headerCell.append($("<th colspan='2'>").text("Command in progress (" + output.duration.toFixed(1) + " seconds so far)"));
    }
    table.append(headerCell);

    for (const [index, x] of output.messages.entries()) {
        var messagesRow = $("<tr>");
        if (index == 0) {
            messagesRow.append($("<td style='text-align:left'>").text("Messages:"));
        } else {
            messagesRow.append($("<td style='text-align:left'>").text(""));
        }
        messagesRow.append($("<td style='text-align:left'>").text(x[1]))
        table.append(messagesRow);
    }

    if (output.state == "Done" || output.state == "Warning") {
        var resultRow = $("<tr>");
        resultRow.append($("<td style='text-align:left'>").text("Result:"))
        if (isObject(output.result)) {
            resultRow.append($("<td style='text-align:left'>").append(
                $("<a>", {
                    text: output.result.name,
                    download: output.result.name,
                    href: 'data:application/octet-stream;base64,' + output.result.contents
                })
            ));
        } else {
            resultRow.append($("<td style='text-align:left'>").text(output.result));
        }
        table.append(resultRow);
    }

    return table;
}

function generateDeviceInfoTable(uuid, device, html_table) {
    $.ajax({
        type: "GET",
        url: `/api/v1/boards/${uuid}/devices`,
        success: function (response) {
            response = response[device];
            html_table.empty();

            // 1) Display C++ type
            var tbody = $("<tbody>");
            var typeRow = $("<tr>");
            typeRow.append($("<td>").text("C++ type"));
            typeRow.append($("<td>").append($("<code>").text(response.type)));
            tbody.append(typeRow);

            // 2) Summarise current state (i.e. FSM & running actions)
            var actionsRow = $("<tr>");
            actionsRow.append($("<td>").text("Current state"));
            if (response.fsm && (response.runningActions.length > 0)) {
                actionsRow.append($("<td>").text("In FSM '" + response.fsm + "', state '" + response.state + "'; running " + response.runningActions[0]));
            } else if (response.fsm) {
                actionsRow.append($("<td>").text("In FSM '" + response.fsm + "', state '" + response.state + "'"));
            } else if (response.runningActions.length > 0) {
                actionsRow.append($("<td>").text("Running command " + runningActions[0]));
            }
            else {
                actionsRow.append($("<td>").append($("<i>").text("No actions running")));
            }
            tbody.append(actionsRow);

            // 3) State address table (if defined)
            if (response.addressTable.length > 0) {
                var row = $("<tr>");
                row.append($("<td>").text("Address table"));
                row.append($("<td>").text(response.addressTable));
                tbody.append(row);
            }

            // 4) State URI (if defined)
            if (response.uri.length > 0) {
                var row = $("<tr>");
                row.append($("<td>").text("URI"));
                row.append($("<td>").text(response.uri));
                tbody.append(row);
            }

            html_table.append(tbody);
        }
    });
}

function deepEqual(object1, object2) {
  const keys1 = Object.keys(object1);
  const keys2 = Object.keys(object2);

  if (keys1.length !== keys2.length) {
    return false;
  }

  for (const key of keys1) {
    const val1 = object1[key];
    const val2 = object2[key];
    const areObjects = isObject(val1) && isObject(val2);
    if (
      areObjects && !deepEqual(val1, val2) ||      !areObjects && val1 !== val2
    ) {
      return false;
    }
  }

  return true;
}

function isObject(object) {
  return object != null && typeof object === 'object';
}

function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

function getApplicationResponse(response, deep=false) {
    if (Array.isArray(response)) {
        devices1 = Object.keys(response[0].data);
        object1 = response[0].data;
        for (board of response) {
            if (!arraysEqual(devices1, Object.keys(board.data))) {
                return;
            }
            if (deep) {
                if (!deepEqual(object1, board.data)) {
                    return;
                }
            }
        }
        response = response[0].data;
    }
    return response;
}

function getProgressFromResponse(response) {
    progress = 1;
    if (Array.isArray(response)) {
        for (board of response) {
            if (board.data.progress < progress) {
                progress = board.data.progress;
            }
        }
    } else {
        progress = response.progress;
    }
    return progress;
}

function getRunningFromResponse(response) {
    running = false;
    if (Array.isArray(response)) {
        for (board of response) {
            if (board.data.state == "Running" || response.state == "Scheduled") {
                running = true;
            }
        }
    } else {
        if (response.state == "Running" || response.state == "Scheduled") {
            running = true;
        }
    }
    return running;
}

function dec2hex (dec) {
  return dec.toString(16).padStart(2, "0")
}

function generateId (len) {
  var arr = new Uint8Array((len || 40) / 2)
  window.crypto.getRandomValues(arr)
  return Array.from(arr, dec2hex).join('')
}
