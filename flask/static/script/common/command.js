function generateDeviceList(ajax_base_url) {
    var device_list = $("#command_form_device_list");
    var checkbox_type = "radio";
    var devices = [];
    $.get({
        url: ajax_base_url + "devices",
        success: function (response) {
            response = getApplicationResponse(response);
            if (response == null) {
                return;
            } else {
                for (var device in response) {
                    device_list.append(
                        `<li>
                            <div class="form-ext-control form-ext-radio">
                                <input class="device-checkbox form-ext-input" type="radio" name="device" value="${device}" id="radio-${device}"/>
                                <label class="form-ext-label" for="radio-${device}">${device}</label>
                            </div>
                        </li>`
                    );
                }
            }
        }
    });
}

function deviceCheckboxClick(ajax_url) {
    $("body").on("click", ".device-checkbox", function () {
        var this_check = $(this);
        this_check.closest("form").find('input.btn-primary').prop("disabled", true);
        if (this_check.is(':checked')) {
            this_check.closest('div.row').next().next().find('table').empty();
            this_check.closest('div.row').next().find('select').children().remove().end();
            this_check.closest('div.row').next().find('select').html("<option hidden disabled selected value> -- select an option -- </option>");
            $.get({
                url: ajax_url,
                success: function (response) {
                    response = getApplicationResponse(response, deep=true);
                    device = this_check.parent()[0].children[1].innerText;
                    response = response[device];
                    for (var command in response) {
                        this_check.closest('div.row').next().find('select').append(new Option(command, command))
                    }
                }
            });
        }
    });
}

function commandDropDownMenu(url) {
    $("body").on("change", ".command-selector", function() {
        var selector = $(this)
        var command = this.value
        var device = "";
        selector.closest('div.row').prev().find('input').each(function () {
            var this_check = $(this);
            if (this_check.is(':checked')) {
                device = this_check.parent()[0].children[1].innerText
            }
        });
        var config_table = selector.closest('div.row').next().find('table')[0];
        $(config_table).empty();
        getConfigTable(device, command, config_table, url);
        selector.closest("form").find('input.btn-primary').prop("disabled", false);
    });
}


function commandError(error) {
    var toast = $($("<div>", {class: "toast toast--warning toast--translucent animated fadeIn"}).append(
        $("<h6>", {text: `Error ${error.status}`})
    ).append(
        $("<p>", {text: error.responseText})
    ));
    toast.insertAfter("div.content#Commands form");
    setTimeout(function() {
        toast.remove();
    }, 10000);
}

function commandSendForm(parsing_func, base_url) {
    $("#command_form").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var output = form.closest('div').find('div.output');
        var button = form.find('input.btn-primary');
        button.prop("disabled", true);
        output.empty();
        var formData = new FormData(this);
        var formObject = {};
        formData.forEach((value, key) => formObject[key] = value);
        var device = formObject.device;
        delete formObject.device;
        var command = formObject.command;
        delete formObject.command;
        // console.log(formObject);
        $("#command-progress").val(0);
        $.get({
            url: base_url + "commands",
            success: async function (response) {
                var requestObject = {
                    'parameters': {}
                };
                response = getApplicationResponse(response, deep=true);
                response = response[device][command][2];
                for (option in response) {
                    var form_value = formObject[`${command}${option}`];
                    switch (response[option][0]) {
                        case "bool":
                            requestObject['parameters'][option] = form_value == "true";
                            break;
                        case "uint":
                            requestObject['parameters'][option] = parseInt(form_value);
                            break;
                        case "file":
                            file_option = option;
                            if (form_value.size == 0) {
                                requestObject['parameters'][file_option] = {path: ""};
                            } else {
                                requestObject['parameters'][file_option] = {name: form_value.name};
                                requestObject['parameters'][file_option]['contents'] = await getBase64(form_value);
                            }
                            break;
                        default:
                            requestObject['parameters'][option] = form_value;
                            break;
                    }
                }
                // console.log(requestObject);
                $.ajax({
                    url: base_url + `commands/${device}/${command}`,
                    method: 'PUT',
                    contentType: "application/json",
                    headers: {lease: localStorage.getItem(`lease_${uuid}`)},
                    data: JSON.stringify(requestObject),
                    success: function (response) {
                        var progress = 0;
                        var running = true;
                        function poll(){
                            if (progress < 1 && running) {
                                $.ajax({
                                    url: base_url + `commands/${device}/${command}`,
                                    success: function(response) {
                                        progress = getProgressFromResponse(response);
                                        $("#command-progress").val(progress);
                                        running = getRunningFromResponse(response);
                                        if (running == false) {
                                            parsing_func(response, output);
                                            button.prop("disabled", false);
                                        }
                                    },
                                    dataType: "json",
                                    complete: setTimeout(poll, 1000),
                                    timeout: 2000,
                                    error: function (error, status, httpError) {
                                        running = false;
                                        commandError(error);
                                        button.prop("disabled", false);
                                    }
                                });
                            }
                        }
                        poll();
                    },
                    error: function (error, status, httpError) {
                        commandError(error);
                        button.prop("disabled", false);
                    }
                });
            },
            error: function (error, status, httpError) {
                commandError(error);
                button.prop("disabled", false);
            }
        });
    });
}
