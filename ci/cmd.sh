#!/bin/sh

rm -rf /var/staticfiles/*
cp -r /flask/static/* /var/staticfiles/

cd /flask
if [ "$ENVIRONMENT" = "development" ]
then
    python3 app.py
else
    gunicorn app:app --bind 0.0.0.0:5000
fi
